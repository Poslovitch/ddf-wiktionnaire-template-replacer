import sbt._

object Version {
  lazy val scalaVersion = "2.13.8"
  lazy val scalaTest = "3.2.12"
  lazy val scalaParserCombinators = "2.1.1"
}

object Dependencies {
  lazy val scalaTest = "org.scalatest" %% "scalatest" % Version.scalaTest
  lazy val scalaParserCombinators = "org.scala-lang.modules" %% "scala-parser-combinators" % Version.scalaParserCombinators
}