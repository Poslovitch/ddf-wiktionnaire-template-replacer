package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateDateSpec extends AnyFlatSpec with Matchers {
  "TemplateDate" should "replace the template properly if it has content" in {
    val input1 = "{{date|1823}} Nom donné par le minéralogiste Kupffer"
    val input2 = "{{date|1477-1478}} Attesté d’abord sous la forme ''actrixe'', ensuite {{date|1503}} ''auctrixe''"
    TemplateReplacer.replaceTemplates(input1) shouldEqual "(1823) Nom donné par le minéralogiste Kupffer"
    TemplateReplacer.replaceTemplates(input2) shouldEqual "(1477-1478) Attesté d’abord sous la forme ''actrixe'', ensuite (1503) ''auctrixe''"
  }

  it should "ignore the template if it has no date inside" in {
    val input = "{{date|lang=fr}} je ne connais pas cette date"
    TemplateReplacer.replaceTemplates(input) shouldEqual "je ne connais pas cette date"
  }

  it should "ignore the template if it has no content" in {
    val input = "Je suis une {{date}} vide !"
    TemplateReplacer.replaceTemplates(input) shouldEqual "Je suis une vide !"
  }

  it should "replace the template but ignore the lang param if it has a date" in {
    // It shouldn't be needed with TemplateReplacer superseding the previous code, but it's here to catch any regression!
    val input1 = "{{date|1823|lang=frm}} Nom donné par le minéralogiste Kupffer"
    val input2 = "{{date|lang=grc|1824}} Nom donné par le minéralogiste Kupffer"
    TemplateReplacer.replaceTemplates(input1) shouldEqual "(1823) Nom donné par le minéralogiste Kupffer"
    TemplateReplacer.replaceTemplates(input2) shouldEqual "(1824) Nom donné par le minéralogiste Kupffer"
  }
}

