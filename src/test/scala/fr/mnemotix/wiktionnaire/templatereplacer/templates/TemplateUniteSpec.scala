package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateUniteSpec extends AnyFlatSpec with Matchers {
  "TemplateUnite" should "ignore the template if no value is provided" in {
    val input = "{{unité}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual ""
  }

  it should "handle a value and properly format numbers" in {
    val input1 = "{{Unité|1234567}}"
    val input2 = "{{Unité|1234567.89}}"
    TemplateReplacer.replaceTemplates(input1) shouldEqual "1 234 567"
    TemplateReplacer.replaceTemplates(input2) shouldEqual "1 234 567,89"
  }

  it should "handle a power of ten" in {
    val input = "{{Unité|1.23456789|e=15}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "1,23456789×10<sup>15</sup>"
  }

  it should "handle a value and a unit" in {
    val input = "{{Unité|10000|km/h}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "10 000 km/h"
  }

  it should "handle a value with a unit and an exposant" in {
    val input = "{{Unité|95|cm|3}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "95 cm<sup>3</sup>"
  }

  it should "handle two units, one of them without exposant" in {
    val input = "{{Unité|10000|km||h|-1}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "10 000 km⋅h<sup>-1</sup>"
  }

  it should "handle even more units" in {
    val input = "{{Unité|10000|J|2|K|3|s|-1}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "10 000 J<sup>2</sup>⋅K<sup>3</sup>⋅s<sup>-1</sup>"
  }

  it should "handle more units and some of them without exposants" in {
    val input = "{{Unité|10000|J||kg||m|-2}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "10 000 J⋅kg⋅m<sup>-2</sup>"
  }

  it should "handle the english number notation, a power of ten, and units with exposants" in {
    val input = "{{Unité|1.23456|e=9|J|2|K|3|s|-1}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "1,23456×10<sup>9</sup> J<sup>2</sup>⋅K<sup>3</sup>⋅s<sup>-1</sup>"
  }

  // DDFS-122: make sure it works on this example
  it should "handle the template in the definition of canne de combat" in {
    val input = "[[sport de combat|Sport de combat]] opposant deux adversaires équipés chacun d’une [[canne]] en bois de [[châtaignier]] longue de {{unité|95|cm}}."
    TemplateReplacer.replaceTemplates(input) shouldEqual "[[sport de combat|Sport de combat]] opposant deux adversaires équipés chacun d’une [[canne]] en bois de [[châtaignier]] longue de 95 cm."
  }
}
