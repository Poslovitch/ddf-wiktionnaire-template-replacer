package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateCalqueSpec extends AnyFlatSpec with Matchers {
  "TemplateCalque" should "parse simple calque du" in {
    val interdetroit = "calque du {{calque|zh|fr}} [[海峡两岸]] (simplifié)"
    TemplateReplacer.replaceTemplates(interdetroit) shouldEqual "calque du chinois [[海峡两岸]] (simplifié)"
  }

  it should "parse with mot= in model" in {
    val IJselHollandais = "Calque du {{calque|nl|fr|mot=Hollandse IJssel}}."
    TemplateReplacer.replaceTemplates(IJselHollandais) shouldEqual "Calque du néerlandais Hollandse IJssel."
  }

  it should "parse without the mot= in model" in {
    val saintPaul = "Calque du {{calque|pt|fr|São Paulo}}."
    TemplateReplacer.replaceTemplates(saintPaul) shouldEqual "Calque du portugais São Paulo."
  }

  it should "parse with 5 params" in {
    val fenggong = "calque partiel du {{calque|zh|fr|風鑼|fēngluó}}"
    TemplateReplacer.replaceTemplates(fenggong) shouldEqual "calque partiel du chinois 風鑼 fēngluó"
  }

  it should "parse with 6 params" in {
    val filsduciel = "Calque du {{calque|zh|fr|天子|tiānzǐ|fils du ciel}}"
    TemplateReplacer.replaceTemplates(filsduciel) shouldEqual "Calque du chinois 天子 tiānzǐ (« fils du ciel »)"
  }
}

