package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateReferenceNecessaireSpec extends AnyFlatSpec with Matchers {
  "TemplateFormePronominale" should "parse simple refnec" in {
    val cabourd = "{{refnec|lang=fr|De l'occitan capbord.}}"
    TemplateReplacer.replaceTemplates(cabourd) shouldEqual "De l'occitan capbord."
  }

  it should "already parse by another parser refnec" in {
    val placomusophile = """{{refnec|lang=fr|1=Forgé sur l'expression « <a href="/form/plaque de muselet">plaque de muselet</a> » et du suffixe grec <a href="/form/φίλος">φίλος</a>, phílos « aimer ».}}"""
    TemplateReplacer.replaceTemplates(placomusophile) shouldEqual """Forgé sur l'expression « <a href="/form/plaque de muselet">plaque de muselet</a> » et du suffixe grec <a href="/form/φίλος">φίλος</a>, phílos « aimer »."""
  }

  it should "ignore the template if there's no sentence in it" in {
    val input = "{{référence nécessaire|lang=fr}} Ceci est peut-être faux."
    TemplateReplacer.replaceTemplates(input) shouldEqual "Ceci est peut-être faux."
  }
}
