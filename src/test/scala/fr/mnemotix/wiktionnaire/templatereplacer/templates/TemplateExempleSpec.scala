package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateExempleSpec extends AnyFlatSpec with Matchers {
  "TemplateExemple" should "replace the template properly if it has content" in {
    val input = "{{exemple|Ceci est un exemple|lang=fr}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "Ceci est un exemple"
  }

  it should "ignore the template if it has no content" in {
    val input = "{{exemple|lang=fr}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual ""
  }
}
