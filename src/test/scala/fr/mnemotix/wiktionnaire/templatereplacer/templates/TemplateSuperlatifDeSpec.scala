package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateSuperlatifDeSpec extends AnyFlatSpec with Matchers {
  "TemplateSuperlatifDe" should "replace the template properly if it has content" in {
    val brillant = "{{superlatif de|brillant|fr}}."
    val fourbe = "{{superlatif de|fourbe|fr}}."
    TemplateReplacer.replaceTemplates(brillant) shouldBe "Superlatif de [[brillant]]."
    TemplateReplacer.replaceTemplates(fourbe) shouldBe "Superlatif de [[fourbe]]."
  }
}
