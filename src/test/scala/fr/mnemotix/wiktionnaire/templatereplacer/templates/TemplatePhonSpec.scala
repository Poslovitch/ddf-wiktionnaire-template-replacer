package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplatePhonSpec extends AnyFlatSpec with Matchers {
  "TemplatePhon" should "replace the template properly if it has content" in {
    val input1 = "#* ''Le français québécois possède des '''diphtongues''' ; par exemple, le mot ''seize'' se prononce ''{{phon|saɛ̯z|lang=fr}}''.''"
    val input2 = "''La prononciation peut n’être pas {{phon|pa.ʁi|fr}}.''"
    TemplateReplacer.replaceTemplates(input1) shouldEqual "#* ''Le français québécois possède des '''diphtongues''' ; par exemple, le mot ''seize'' se prononce ''saɛ̯z''.''"
    TemplateReplacer.replaceTemplates(input2) shouldEqual "''La prononciation peut n’être pas pa.ʁi.''"
  }

  it should "ignore the template if it has no content" in {
    val input = "Je suis une {{phon}} vide !"
    TemplateReplacer.replaceTemplates(input) shouldEqual "Je suis une vide !"
  }
}

