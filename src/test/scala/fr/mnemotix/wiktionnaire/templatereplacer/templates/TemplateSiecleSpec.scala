package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateSiecleSpec extends AnyFlatSpec with Matchers {
  "TemplateSiecle" should "handle a single century with nothing before or after" in {
    val input1 = "{{siècle|XXI}}"
    val input2 = "{{siècle|XVIII}}"
    TemplateReplacer.replaceTemplates(input1) shouldEqual "(XXIe siècle)"
    TemplateReplacer.replaceTemplates(input2) shouldEqual "(XVIIIe siècle)"
  }

  it should "handle a single century with words before" in {
    val input1 = "{{siècle|Début XXI}}"
    val input2 = "{{siècle|Milieu XVIII}}"
    TemplateReplacer.replaceTemplates(input1) shouldEqual "(Début XXIe siècle)"
    TemplateReplacer.replaceTemplates(input2) shouldEqual "(Milieu XVIIIe siècle)"
  }

  it should "handle a single century with words after" in {
    val input1 = "{{siècle|XXI E.C.}}"
    val input2 = "{{siècle|XVIII ap. J.-C.}}"
    TemplateReplacer.replaceTemplates(input1) shouldEqual "(XXIe siècle E.C.)"
    TemplateReplacer.replaceTemplates(input2) shouldEqual "(XVIIIe siècle ap. J.-C.)"
  }

  it should "handle a single century with words before and after" in {
    val input1 = "{{siècle|Vers le XI av. J.-C.}}"
    val input2 = "{{siècle|Fin VII E.C.}}"
    TemplateReplacer.replaceTemplates(input1) shouldEqual "(Vers le XIe siècle av. J.-C.)"
    TemplateReplacer.replaceTemplates(input2) shouldEqual "(Fin VIIe siècle E.C.)"
  }

  it should "handle a simple range of centuries" in {
    val input1 = "{{siècle|XX|XXI}}"
    val input2 = "{{siècle|XIV|XVIII}}"
    TemplateReplacer.replaceTemplates(input1) shouldEqual "(XXe siècle – XXIe siècle)"
    TemplateReplacer.replaceTemplates(input2) shouldEqual "(XIVe siècle – XVIIIe siècle)"
  }

  it should "handle a range of centuries, with any of them having words before and/or after" in {
    val input1 = "{{siècle|Fin XX|début XXI}}"
    val input2 = "{{siècle|Milieu du VII av. J.-C.|Fin VI av. J.-C.}}"
    TemplateReplacer.replaceTemplates(input1) shouldEqual "(Fin XXe siècle – début XXIe siècle)"
    TemplateReplacer.replaceTemplates(input2) shouldEqual "(Milieu du VIIe siècle av. J.-C. – Fin VIe siècle av. J.-C.)"
  }

  it should "handle a single century with doubt" in {
    val input1 = "{{siècle|XXI|doute=oui}}"
    val input2 = "{{siècle|doute=1|XVIII}}"
    TemplateReplacer.replaceTemplates(input1) shouldEqual "(XXIe siècle ?)"
    TemplateReplacer.replaceTemplates(input2) shouldEqual "(XVIIIe siècle ?)"
  }

  it should "handle a single centuries with doute=non" in {
    val input1 = "{{siècle|XXI|doute=non}}"
    TemplateReplacer.replaceTemplates(input1) shouldEqual "(XXIe siècle)"
  }

  it should "handle a range of century with doubt" in {
    val input1 = "{{siècle|XX|XXI|doute=oui}}"
    TemplateReplacer.replaceTemplates(input1) shouldEqual "(XXe siècle – XXIe siècle ?)"
  }

  it should "handle a range of centuries, with words and with doubt" in {
    val input1 = "{{siècle|Fin XX|début XXI|doute=1}}"
    TemplateReplacer.replaceTemplates(input1) shouldEqual "(Fin XXe siècle – début XXIe siècle ?)"
  }

  it should "handle the first century" in {
    val input = "{{siècle|I}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "(Ier siècle)"
  }
}
