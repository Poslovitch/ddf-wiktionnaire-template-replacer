package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateFormePronominaleSpec extends AnyFlatSpec with Matchers {
  "TemplateFormePronominale" should "replace the template properly if it has content" in {
    val input = "{{forme pronominale|enhardir}}, trouver du courage pour agir"
    TemplateReplacer.replaceTemplates(input) shouldEqual "Forme pronominale de [[enhardir]], trouver du courage pour agir"
  }
}
