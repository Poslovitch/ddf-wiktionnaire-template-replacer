package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateSiecle2Spec extends AnyFlatSpec with Matchers {
  "TemplateSiecle2" should "handle a non-specific century in roman numerals" in {
    val input1 = "{{siècle2|XXI}}"
    val input2 = "{{siècle2|XVIII}}"
    TemplateReplacer.replaceTemplates(input1) shouldEqual "XXIe"
    TemplateReplacer.replaceTemplates(input2) shouldEqual "XVIIIe"
  }

  it should "convert a century from arabic to roman numerals" in {
    val input = "{{siècle2|2}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "IIe"
  }

  it should "ignore the template if empty" in {
    val input = "{{siècle2}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual ""
  }

  it should "ignore the template if the value in arabic numbers cannot be converted to roman numerals" in {
    val input = "{{siècle2|-1}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual ""
  }

  it should "handle the 'Ier' century" in {
    val input1 = "{{siècle2|1}}"
    val input2 = "{{siècle2|I}}"
    TemplateReplacer.replaceTemplates(input1) shouldEqual "Ier"
    TemplateReplacer.replaceTemplates(input2) shouldEqual "Ier"
  }
}
