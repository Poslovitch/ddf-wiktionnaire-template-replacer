package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateFchimSpec extends AnyFlatSpec with Matchers {
  "TemplateFchim" should "handle a basic chemical formula" in {
    val input = "{{fchim|H|2|O}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "H<sub>2</sub>O"
  }

  it should "handle a formula with an escaped equal sign in it" in {
    val input = "{{fchim|CH|2|&#61;CH|2}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "CH<sub>2</sub>&#61;CH<sub>2</sub>"
  }

  it should "handle a formula with no indices" in {
    val input = "{{fchim|C||H}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "CH"
  }

  it should "handle up to 20 parameters (10 elements, 10 indices)" in {
    // K-11 will be ignored
    val input = "{{fchim|A|1|B|2|C|3|D|4|E|5|F|6|G|7|H|8|I|9|J|10|K|11}}"
    TemplateReplacer.replaceTemplates(input) shouldEqual "A<sub>1</sub>B<sub>2</sub>C<sub>3</sub>D<sub>4</sub>E<sub>5</sub>F<sub>6</sub>G<sub>7</sub>H<sub>8</sub>I<sub>9</sub>J<sub>10</sub>"
  }

  // DDFS-214 - Make sure it can handle complex cases that failed with the Wiktionnaire Annotator
  it should "handle all the chemical formulas in the 'acide tellureux' definition" in {
    val fchim1 = "{{fchim|H|2|TeO|3}}"
    // the following formulas also use {{e}}, and any regression on this template should make this test fail
    val fchim2 = "{{fchim|HTeO|3}}{{e|-}}"
    val fchim3 = "{{fchim|TeO|3}}{{e|2-}}"

    TemplateReplacer.replaceTemplates(fchim1) shouldEqual "H<sub>2</sub>TeO<sub>3</sub>"
    TemplateReplacer.replaceTemplates(fchim2) shouldEqual "HTeO<sub>3</sub><sup>-</sup>"
    TemplateReplacer.replaceTemplates(fchim3) shouldEqual "TeO<sub>3</sub><sup>2-</sup>"
  }
}

