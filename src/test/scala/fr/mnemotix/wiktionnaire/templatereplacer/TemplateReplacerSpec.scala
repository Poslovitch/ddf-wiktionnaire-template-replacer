package fr.mnemotix.wiktionnaire.templatereplacer

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TemplateReplacerSpec extends AnyFlatSpec with Matchers {
  "The TemplateReplacer" should "replace a single template" in {
    val input1 = "{{lien|wololo|lang=fr}}"
    val input2 = "{{w}}"
    val input3 = "foo bar {{lang|si vis pacem, para bellum|la}} foo bar !"

    val result1 = TemplateReplacer.replaceTemplates(input1)
    val result2 = TemplateReplacer.replaceTemplates(input2)
    val result3 = TemplateReplacer.replaceTemplates(input3)

    assert(!(result1.contains("{{") && result1.contains("}}")))
    assert(!(result2.contains("{{") && result2.contains("}}")))
    assert(!(result3.contains("{{") && result3.contains("}}")))
  }

  it should "ignore malformed templates" in {
    val input1 = "{{lien|wololo|fr"
    val input2 = "{{siècle2|xx}"

    val result1 = TemplateReplacer.replaceTemplates(input1)
    val result2 = TemplateReplacer.replaceTemplates(input2)

    result1 shouldEqual input1
    result2 shouldEqual input2
  }

  it should "replace a multi-line template" in {
    // I use a template that the TemplateReplacer *will* recognize (here: TemplateLien)
    val input1 = """{{lien
                   ||papaoutai
                   ||lang=fr}}""".stripMargin

    val input2 = """{{lien
                   ||papaoutai
                   ||lang=fr
                   |}}""".stripMargin

    val result1 = TemplateReplacer.replaceTemplates(input1)
    val result2 = TemplateReplacer.replaceTemplates(input2)

    // To make sure the template has been replaced, we check:
    //  1) that there is not a single "double curly braces" remaining
    //  2) that the TemplateReplacer did not consider the template's name to be "lien\n" instead of "lien"
    // FIXME: Make this more robust?
    assert(!(result1.contains("{{") && result1.contains("}}")) && !result1.contains("!!undefined:"))
    assert(!(result2.contains("{{") && result2.contains("}}")) && !result2.contains("!!undefined:"))
  }

  it should "replace a template with leading/trailing spaces around parameters" in {
    val input1 = "{{lien | 1=wololo |lang=fr }}"
    val input2 = "{{      lien | wololo |         lang=fr }}"

    val result1 = TemplateReplacer.replaceTemplates(input1)
    val result2 = TemplateReplacer.replaceTemplates(input2)

    assert(!(result1.contains("{{") && result1.contains("}}")) && !result1.contains("!!undefined:"))
    assert(!(result2.contains("{{") && result2.contains("}}")) && !result2.contains("!!undefined:"))
  }

  it should "remove any empty template ({{}})" in {
    val input1 = "{{}}"
    val input2 = "this is a sentence with an {{}} empty template"
    val input3 = "even this template is empty: {{ }}"

    TemplateReplacer.replaceTemplates(input1) shouldEqual ""
    TemplateReplacer.replaceTemplates(input2) shouldEqual "this is a sentence with an empty template"
    TemplateReplacer.replaceTemplates(input3) shouldEqual "even this template is empty:"
  }

  it should "handle a template with an inner link with a display title" in {
    val input1 = "{{Lang|en|[[English#en|English]]}}"
    val input2 = "{{term|[[encan|Encan]]}}"

    TemplateReplacer.replaceTemplates(input1) should contain ('|') // the pipe in the link shouldn't have been touched
    TemplateReplacer.replaceTemplates(input2) should contain ('|') // same here
  }

  it should "handle a template with some of its positional parameters having blank values" in {
    // TODO: Find a way to test this
    succeed
  }

  it should "handle a template with some of its named parameters having blank values" in {
    // TODO: Find a way to test this
    succeed
  }

  //TODO: Try with "{{lang|si vis pacem, para bellum|lang=la}}": why???
  //TODO: Escape the "equal" signs that innermost templates may generate:
  // {{refnec|lang=fr|Forgé sur l'expression « <a href="/form/plaque de muselet">plaque de muselet</a> » et du suffixe grec <a href="/form/φίλος">φίλος</a>, phílos « aimer ».}}
}
