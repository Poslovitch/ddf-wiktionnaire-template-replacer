package fr.mnemotix.wiktionnaire.templatereplacer.utils

case class RomanNumeralsSpec()
//TODO: faire des tests pour éviter toute régression de la regex
// Milieu du VII avant Jésus-C<-hrist.
// XXI de l'<-ère commune.
// D<-ébut XXI
// Fin XI av. J.-C<-.
// IV E.C<-.