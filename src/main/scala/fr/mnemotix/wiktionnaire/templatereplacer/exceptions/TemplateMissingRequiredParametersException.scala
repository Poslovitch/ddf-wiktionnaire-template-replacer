package fr.mnemotix.wiktionnaire.templatereplacer.exceptions

/**
 * Exception thrown when not all the required parameters of a Template are passed at instantiation time.
 */
class TemplateMissingRequiredParametersException extends TemplateReplacerException {
}
