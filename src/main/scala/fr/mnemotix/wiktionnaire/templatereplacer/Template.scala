package fr.mnemotix.wiktionnaire.templatereplacer

import fr.mnemotix.wiktionnaire.templatereplacer.exceptions.TemplateMissingRequiredParametersException

/**
 *
 * @param definition
 * @param parameters
 * @throws TemplateMissingRequiredParametersException if some required parameters (from definition) are not in the parameters map
 */
abstract class Template(definition: List[TemplateParameter], private val parameters: Map[String, String]) {
  /**
   * The minimum size params should be in order to fulfill the amount of required parameters
   */
  val minimumSize: Int = definition.foldLeft(0)((sum, param) => sum + (if (param.required) 1 else 0) )

  if (parameters.size < minimumSize) throw new TemplateMissingRequiredParametersException()

  // Manage aliases
  private val tmpParams = scala.collection.mutable.Map[String, String]()
  tmpParams ++= parameters
  for (key <- parameters.keys) {
    definition.find(_.aliases.contains(key)).foreach(tParam => tmpParams.addOne(tParam.key -> tmpParams(key)))
  }

  val params: Map[String, String] = tmpParams.toMap

  // now that aliases are handled, params should contain all the "keys" of the definition's template parameters
  // we count the amount of required template parameters that are not found in params
  if (definition.count(tp => tp.required && !params.contains(tp.key)) != 0)
    throw new TemplateMissingRequiredParametersException() // if there's a required parameter that's not here, we throw.

  /**
   * Returns the text this template should be replaced with
   * @return the text this template should be replaced with
   */
  def format(): String
}
