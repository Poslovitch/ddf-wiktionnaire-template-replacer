package fr.mnemotix.wiktionnaire.templatereplacer.utils

/**
 * Correspondance-mapping between the name and the display name of sections (template {{S}}).
 * See: https://fr.wiktionary.org/wiki/Module:types_de_mots/data
 */
case object WiktionarySections {
  /**
   * Provides the section's display name based on its name.
   * @param s the name of the section
   * @param locution whether or not this section relates to a locution
   * @return the section's display name based on its name.
   */
  def toDisplayName(s: String, locution: Boolean = false): String = {
    //TODO: Complete the list with https://fr.wiktionary.org/wiki/Module:types_de_mots/data
    s.toLowerCase match {
      case "nom" =>
        if (locution) "Locution nominale"
        else "Nom commun"
      case "nom propre" | "nom-pr" => "Nom propre"
      case "verb" =>
        if (locution) "Locution verbale"
        else "Verbe"
      case "adj" | "adjectif" =>
        if (locution) "Locution adjectivale"
        else "Adjectif"
      case "part" => "Particule"
      case "prép" =>
        if (locution) "Locution prépositive"
        else "Préposition"
      case "pronom" =>
        if (locution) "Locution pronominale"
        else "Pronom"
      case "adv" =>
        if (locution) "Locution adverbiale"
        else "Adverbe"
      case "conj" =>
        if (locution) "Locution conjonctive"
        else "Conjonction"

      // Exclamations
      case "interj" =>
        if (locution) "Locution interjective"
        else "Interjection"
      case "suf" => "Suffixe"
      case "préf" => "Préfixe"
      case "lettre" => "Lettre"
      case _ => ""
    }
  }
}