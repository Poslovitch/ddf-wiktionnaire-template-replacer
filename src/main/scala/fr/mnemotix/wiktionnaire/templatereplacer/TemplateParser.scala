package fr.mnemotix.wiktionnaire.templatereplacer

import scala.annotation.tailrec
import scala.util.parsing.combinator.RegexParsers

object TemplateParser extends RegexParsers {
  private def separator: Parser[Any] = '|'
  // This matches a "template member", i.e. anything separated by separator (pipe) in a template
  // However, the first alternative (\[\[[^\]]*\]\]) is here to prevent matching separators inside double-brackets,
  //   i.e. wikilinks : [[hello|Hello]]. As a result, "This is a [[link#en|link]] to another page" will be matched
  //   as a whole template member, instead of being split into two : "This is a [[link#en" and "link]] to another page"
  private def templateMember: Parser[String] = "(?>\\[\\[[^]]*]]|[^|])*".r

  def template: Parser[Template] = templateMember ~ opt(separator ~> repsep(templateMember, separator)) ^^ { result =>
    val name = result._1
      .replaceAll("[\n\r]+", "") // replace all newlines
      .trim // removes leading/trailing spaces
    val params = result._2

    params match {
      // We have to get rid of any line breaks
      case Some(paramsList) => TemplateFactory.create(
        name,
        populateParams(for (param <- paramsList) yield param.replaceAll("[\n\r]+", "").trim)
      )
      case None => TemplateFactory.create(name, Map())
    }
  }

  /**
   * Populates and returns a Map of the template's parameters from the provided paramsList.
   * @param paramsList
   * @param counter
   * @param parametersMap
   * @return
   */
  @tailrec
  private def populateParams(paramsList: List[String], counter: Int = 1, parametersMap: Map[String, String] = Map()): Map[String, String] = {
    if (paramsList.isEmpty)
      parametersMap
    else {
      val param = paramsList.head
      val containsEqual = param.split("=", 2) // limit = 2 means at least once
      if (containsEqual.length > 1) {
        // it has a "=", so there's a named parameter we have to use

        if (containsEqual(1).isBlank)
          populateParams(paramsList.tail, counter, parametersMap) // there is no value for that param, so we ignore it
        else
          populateParams(paramsList.tail, counter, parametersMap + (containsEqual(0) -> containsEqual(1)))
      } else {
        // we use the positional parameters there

        if (param.isBlank) // it's important to check for param here, so that it can fall in the condition above if param = "something=" (which is "blank" too)
          populateParams(paramsList.tail, counter + 1, parametersMap) // same here, we ignore it but still increase the counter
        else
          populateParams(paramsList.tail, counter + 1, parametersMap + (counter.toString -> param))
      }
    }
  }
}
