package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

/**
 * Modèle:lien
 *
 * @param parameters
 */
case class TemplateLien(parameters: Map[String, String])
  extends Template(
    List(
      TemplateParameter("1", required = true),
      TemplateParameter("2", List("lang")),
      TemplateParameter("3", List("code")),
      TemplateParameter("dif"),
      TemplateParameter("tr"),
      TemplateParameter("sens")
    ),
    parameters) {

  /**
   * Le mot vers lequel le lien doit pointer. Peut contenir des balises de formatage.
   */
  val mot: String = params("1")
  /**
   * Le code de langue du mot.
   * Valeur par défaut: fr
   */
  val lang: String = params.getOrElse("2", "fr")
  /**
   * Le code de la section cible dans la page pointée.
   * Exemple: nom-1, verbe-2, etc.
   */
  val codeSection: Option[String] = params.get("3")
  /**
   * La transcription du mot.
   */
  val texte: Option[String] = params.get("dif")
  /**
   * Une traduction du terme en français.
   */
  val sens: Option[String] = params.get("sens")

  override def format(): String = {
    if (lang.equals("fr"))
      s"[[$mot]]"
    else
      s"${texte.getOrElse(mot)}${if (sens.isDefined) s" (« ${sens.get} »)" else ""}"
  }
}
