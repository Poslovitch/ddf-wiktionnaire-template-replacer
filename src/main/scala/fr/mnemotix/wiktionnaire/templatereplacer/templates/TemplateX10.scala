package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

/**
 * Modèle:x10
 * @param parameters
 */
case class TemplateX10(parameters: Map[String, String])
  extends Template(
    List(
      TemplateParameter("1"),
    ),
    parameters) {

  /**
   * l’exposant de base 10 à indiquer avec le multiplicateur.
   */
  val exposant: Option[String] = params.get("1")

  override def format(): String = "×10" + {
    if (exposant.isDefined) s"{{e|${exposant.get}}}"
    else ""
  }
}
