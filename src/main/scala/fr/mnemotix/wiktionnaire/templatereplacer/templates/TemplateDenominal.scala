package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.utils.{BooleanLiterals, WiktionaryLanguages}
import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

/**
 * Modèle:dénominal
 *
 * @param parameters
 */
case class TemplateDenominal(parameters: Map[String, String])
  extends Template(
    List(
      TemplateParameter("1", aliases = List("lang")), // ignored
      TemplateParameter("2", aliases = List("lang-lien")), // ignored
      TemplateParameter("de"),
      TemplateParameter("texte"),
      TemplateParameter("m"),
      TemplateParameter("nolien"),
      //there's also nocat and clé, but it's ignored
    ),
    parameters) {

  /**
   * Si renseigné, la première lettre sera en majuscule.
   * Défaut : non
   */
  val majuscule: Boolean = BooleanLiterals.toBoolean(params.getOrElse("m", "0"))

  /**
   * Le mot ou la locution à l'origine du mot vedette.
   */
  val de: Option[String] = params.get("de")

  /**
   * Le texte à afficher à la place du paramètre « de », ignoré si le paramètre « nolien » est renseigné.
   */
  val texte: Option[String] = params.get("texte")

  /**
   * Si renseigné, le modèle ne fera pas de lien vers le terme renseigné par le paramètre « de ».
   * Le paramètre « texte » est alors ignoré.
   * Défaut : non
   */
  val noLien: Boolean = BooleanLiterals.toBoolean(params.getOrElse("nolien", "non"))

  override def format(): String = {
    capitalize("dénominal") + {
      if (de.isDefined) {
        if (!noLien && texte.isDefined) s" de [[${de.get}|${texte.get}]]"
        else if (noLien) s" de ${de.get}"
        else s" de [[${de.get}]]"
      }
      else ""
    }
  }

  /**
   * Returns this string with the first letter converted to uppercase if majuscule is true.
   * This string is left unchanged otherwise, or if the first letter is already uppercase.
   * @param s the String to capitalize.
   * @return the capitalized String.
   */
  private def capitalize(s: String): String = {
    if (majuscule) s.capitalize
    else s
  }
}
