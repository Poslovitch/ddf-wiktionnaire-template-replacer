package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

/**
 * Modèle:exemple
 *
 * @param parameters
 */
case class TemplateExemple(parameters: Map[String, String])
  extends Template(
    List(
      TemplateParameter("1", required=true),
      TemplateParameter("lang")
    ),
    parameters) {

  /**
   * Citation dans la langue de l’entrée
   */
  val citation: String = params("1")

  override def format(): String = citation
}
