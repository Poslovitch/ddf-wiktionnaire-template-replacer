package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

/**
 * Modèle:prononciation
 *
 * @param parameters
 */
case class TemplatePrononciation(parameters: Map[String, String])
  extends Template(
    List(
      TemplateParameter("1", required=true),
      TemplateParameter("2", List("lang"))
    ),
    parameters) {

  /**
   * La prononciation en API.
   */
  val prononciation: String = params("1")

  override def format(): String = s"\\\\$prononciation\\\\" // because we're using regexes, the pattern replacement requires these backslashes to be escaped a few more times
}
