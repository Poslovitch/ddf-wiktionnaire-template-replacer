package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.utils.{BooleanLiterals, RomanNumerals}
import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

/**
 * Modèle:siècle
 * @param parameters
 */
case class TemplateSiecle (parameters: Map[String, String])
  extends Template(
    List(
      TemplateParameter("1", required = true),
      TemplateParameter("2"),
      TemplateParameter("doute"),
      TemplateParameter("lang")
    ),
    parameters) {

  /**
   * Un siècle, avec éventuellement une précision en amont et en aval.
   *
   * Exemples :
   * <ul>
   *   <li>Fin XIX</li>
   *   <li>Vers le XI av. J.-C.</li>
   * </ul>
   */
  val siecle: String = params("1")

  /**
   * Un second siècle, de manière à former une fourchette.
   * On peut aussi préciser comme pour siecle.
   *
   * @see TemplateSiecle#siecle
   */
  val siecleFourchette: Option[String] = params.get("2")

  /**
   * Précise que l’information est incertaine (doute=oui, doute=1)
   * Défaut: non
   */
  val doute: String = params.getOrElse("doute", "non")

  /**
   * Précise dans quelle langue la date est manquante quand elle l'est.
   */
  val lang: Option[String] = params.get("lang")

  override def format(): String = {
    val inside = siecleFourchette match {
      case Some(fourchette) => formatSiecle(siecle) + " – " + formatSiecle(fourchette)
      case None => formatSiecle(siecle)
    }

    s"($inside${if (BooleanLiterals.toBoolean(doute)) " ?" else "" })"
  }

  def formatSiecle(s: String): String = RomanNumerals.regex.replaceAllIn(s, m => m.toString().toLowerCase match {
    case "i" => "$0er siècle" // special handling for the "I" century : "Ier siècle", not "Ie siècle"
    case _ => "$0e siècle"
  })
}
