package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

/**
 * Modèle:lang
 *
 * @param parameters
 */
case class TemplateLang(parameters: Map[String, String])
  extends Template(
    List(
      TemplateParameter("1", required = true),
      TemplateParameter("2", List("texte"), required = true),
      TemplateParameter("3", List("tr")),
      TemplateParameter("4", List("sens")),
    ),
    parameters) {

  /**
   * Code de langue IETF du texte inclus
   */
  val lang: String = params("1")
  /**
   * Texte en langue étrangère.
   */
  val texte: String = params("2")
  /**
   * Transcription ou translittération du texte.
   * Attention, ce n’est pas une traduction, mais une conversion lettre à lettre la plus proche possible de l’orthographe originale.
   */
  val transcript: Option[String] = params.get("3")
  /**
   * Signification du texte en français.
   */
  val sens: Option[String] = params.get("4")

  override def format(): String = {
    s"$texte${if (transcript.isDefined) s", ${transcript.get}" else ""}${if (sens.isDefined) s" (« ${sens.get} »)" else ""}"
  }
}
