package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

/**
 * Modèle:date
 *
 * @param parameters
 */
case class TemplateDate(parameters: Map[String, String])
  extends Template(
    List(
      TemplateParameter("1", required=true),
      TemplateParameter("lang")
    ),
    parameters) {

  /**
   * La date
   */
  val date: String = params("1")

  override def format(): String = s"($date)"
}
