package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

case class TemplateReferenceNecessaire(parameters: Map[String, String])
  extends Template(
    List(
      TemplateParameter("1", required=true),
      TemplateParameter("lang")
    ),
    parameters) {

  /**
   * Phrase difficile à avaler
   */
  val phrase: String = params("1")

  override def format(): String = phrase
}
