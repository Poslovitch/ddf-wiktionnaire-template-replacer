package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

/**
 * Modèle:e
 * @param parameters
 */
case class TemplateE(parameters: Map[String, String])
  extends Template(
    List(
      TemplateParameter("1"),
    ),
    parameters) {

  /**
   * le texte à mentionner en exposant (par défaut, écrit « e » en exposant).
   */
  val texte: String = params.getOrElse("1", "e")

  override def format(): String = s"<sup>$texte</sup>"
}
