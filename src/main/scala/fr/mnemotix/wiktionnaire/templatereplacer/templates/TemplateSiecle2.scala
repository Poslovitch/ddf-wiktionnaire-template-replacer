package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.utils.RomanNumerals
import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

/**
 * Modèle:siècle2
 *
 * @param parameters
 */
case class TemplateSiecle2 (parameters: Map[String, String])
  extends Template(
    List(
      TemplateParameter("1", required = true)
    ),
    parameters) {

  /**
   * Le nombre en chiffres arabes ou en chiffres romains (indifférent à la casse)
   */
  val nombre: String = params("1")

  override def format(): String = {
    if ("-?\\d+".r.matches(nombre)) {
      // if this is a century in arabic numbers (2, 715...), we have to convert it to roman numerals then format it
      try {
        formatSiecle(RomanNumerals.fromArabicNumerals(Integer.valueOf(nombre)))
      } catch {
        case _: IllegalArgumentException => "" // if #fromArabicNumerals raises an exception (could not convert), we ignore the template
      }
    }
    else formatSiecle(nombre) // this is most likely roman numerals, so we just format it
  }

  /**
   * Formats the century ('e' or 'er') depending on the passed argument.
   * @param s the century to format
   * @return the century formatted
   */
  def formatSiecle(s: String): String = s.toLowerCase match {
    case "i" => s + "er"
    case _ => s + "e"
  }
}
