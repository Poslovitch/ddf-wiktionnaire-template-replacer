package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

case class TemplateSuperlatifDe(parameters: Map[String, String]) extends Template(
  List(
    TemplateParameter("1", required = true),
    TemplateParameter("2"),
    TemplateParameter("3"),
    TemplateParameter("4")
  ),
  parameters) {

  /**
   * Graphie du radical
   */
  val radical: String = params("1")
  /**
   * Optionnel. Le code de la langue. Le défaut est fr (le français).
   */
  val lang: String = params.getOrElse("2", "fr")
  /**
   * Optionnel. La classe lexicale du radical. adj pour un adjectif et adv pour un adverbe. Le défaut est le premier.
   */
  val section: String = params.getOrElse("3", "adj")
  /**
   * Optionnel. Numéro du radical.
   */
  val sectionNumber: Option[String] = params.get("4")

  /**
   * Returns the text the template should be replaced with
   *
   * @return the text the template should be replaced with
   */
  override def format(): String = s"Superlatif de [[$radical]]"
}


