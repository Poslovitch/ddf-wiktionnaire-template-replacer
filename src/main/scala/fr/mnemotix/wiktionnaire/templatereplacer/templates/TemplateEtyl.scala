package fr.mnemotix.wiktionnaire.templatereplacer.templates

import fr.mnemotix.wiktionnaire.templatereplacer.utils.WiktionaryLanguages
import fr.mnemotix.wiktionnaire.templatereplacer.{Template, TemplateParameter}

/**
 * Modèle:étyl
 *
 * @param parameters
 */
case class TemplateEtyl(parameters: Map[String, String])
  extends Template(
    List(
      TemplateParameter("1", required = true),
      TemplateParameter("2", required = true),
      TemplateParameter("3", aliases = List("mot")),
      TemplateParameter("4", aliases = List("tr", "R")),
      TemplateParameter("5", aliases = List("sens")),
      TemplateParameter("dif")
    ),
    parameters) {

  /**
   * Code de la langue d'origine
   */
  val originLang: String = params("1")
  /**
   * Code de la langue cible (et de la section courante)
   */
  val targetLang: String = params("2")
  /**
   * Le mot d'origine du mot-vedette
   */
  val mot: Option[String] = params.get("3")
  /**
   * La transcription du mot s’il n’utilise pas l’alphabet latin.
   */
  val tr: Option[String] = params.get("4")
  /**
   * Le sens du mot
   */
  val sens: Option[String] = params.get("5")
  /**
   * Le texte à afficher à la place du mot
   */
  val texte: Option[String] = params.get("dif")

  /**
   * Returns the text the template should be replaced with
   *
   * @return the text the template should be replaced with
   */
  override def format(): String = {
    WiktionaryLanguages.langs.getOrElse(originLang, "inconnu") + { // mot or dif=
      if (texte.isDefined) " " + texte.get
      else {
        if (mot.isDefined) " " + mot.get
        else ""
      }
    } + { // tr
      if (tr.isDefined) " " + tr.get
      else ""
    } + { // sens
      if (sens.isDefined) s" (« ${sens.get} »)"
      else ""
    }
  }
}
