import Dependencies._
import com.gilcloud.sbt.gitlab.{GitlabCredentials, GitlabPlugin}
import sbt.ThisBuild


val meta = """META.INF(.)*""".r
enablePlugins(GitlabPlugin)
ThisBuild / useCoursier := false
ThisBuild / onChangedBuildSource := ReloadOnSourceChanges

lazy val root = (project in file("."))
  .settings(
    scalaVersion := Version.scalaVersion,
    organization := "com.mnemotix",
    organizationName := "MNEMOTIX SCIC",
    licenses := List("Apache 2" -> new URL("http://www.apache.org/licenses/LICENSE-2.0.txt")),
    developers := List(
      Developer(
        id = "fcuny",
        name = "Florian Cuny",
        email = "florian.cuny@mnemotix.com",
        url = url("http://www.mnemotix.com")
      ),
      Developer(
        id = "prlherisson",
        name = "Pierre-René Lherisson",
        email = "pr.lherisson@mnemotix.com",
        url = url("http://www.mnemotix.com")
      )
    ),

    name := "ddf-wiktionnaire-template-replacer",
    description := "\n\nProof of Concept d'un \"remplaceur de modèles\" pour les textes issus du Wiktionnaire, qui utilise objets et pattern combinators pour faciliter la maintenance au long terme.",
    homepage := Some(url("https://gitlab.com/mnemotix/ddf/ddf-wiktionnaire-template-replacer")),
    scmInfo := Some(
      ScmInfo(
        url("https://gitlab.com/mnemotix/ddf/ddf-wiktionnaire-template-replacer.git"),
        "scm:git@gitlab.com:mnemotix/ddf/ddf-wiktionnaire-template-replacer.git"
      )
    ),
    credentials += Credentials(Path.userHome / ".sbt" / ".credentials.gitlab"),
    GitlabPlugin.autoImport.gitlabGroupId := Some(6257667),
    GitlabPlugin.autoImport.gitlabProjectId := Some(35329974),
    GitlabPlugin.autoImport.gitlabDomain := "gitlab.com",
    //  updateOptions := updateOptions.value.withGigahorse(false),
    resolvers ++= Seq(
      Resolver.mavenLocal,
      Resolver.sonatypeRepo("public"),
      Resolver.typesafeRepo("releases"),
      Resolver.sbtPluginRepo("releases"),
      "gitlab-maven" at "https://gitlab.com/api/v4/projects/35329974/packages/maven"
    ),
    libraryDependencies ++= Seq(
      scalaTest % Test,
      scalaParserCombinators
    )
  )